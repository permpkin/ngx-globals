import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class Globals {

	private Data:any = {};
	private subscriber: Subscription = new Subscription();

	constructor() { }

	wipe():void {

        Object.keys(this.Data).forEach((Key)=>{

        	this.clear(Key);

        })

	}

	clear( key:string ):void {

        this.Data[key].next(false);

	}

	get( key:string ):string {

        return this.has(key) ? this.Data[key].getValue() : false;

	}

	has( key:string ):boolean {

        return this.Data.hasOwnProperty(key) && this.Data[key] != false;

	}

	observe( key:string ): Observable<any> {

		if( !this.has(key) ) this.Data[key] = new BehaviorSubject<any>(false);

        return this.Data[key].asObservable();

	}

	set( key:string, value:any ):void {

		if( !this.has(key) )

			this.Data[key] = new BehaviorSubject<any>(value);

		this.Data[key].next(value);

	}

	subscribe( subscription:any ) {

		this.subscriber.add(subscription);

	}

	unsubscribe() {

		this.subscriber.unsubscribe();

	}

}