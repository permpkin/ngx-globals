# Angular 2+ Globals Service #

Basic Global Variable Service ( With Observables/Behaviour Subjects ).

## Globals Service Usage

getting and setting variables
```typescript
import { Component, OnInit } from '@angular/core';
import { Globals } from 'ngx-globals';

@Component({
    template: '<p>Component Contents</p>'
})

export class SomeComponent implements OnInit {

	constructor( private Globals:Globals ) {}

	ngOnInit(){

		// returns variable if exists, else returns false
		let SomeVariable = this.Globals.get('SomeVariable');

		// sets variable and triggers behavioursubject change ( sent to subscribers, see below )
		this.Globals.set('SomeVariable',{anytype:'ofdata'})

		// removes variable and destroys all subscriptions to that variable
		this.Globals.clear('SomeVariable')

		// returns true/false whether variable is set ( unless variable value is set as 'false' )
		this.Globals.has('SomeVariable')
		
		// clears all variables, subscriptions ( useful for functions such as logout functions )
		this.Globals.wipe()

	}

}
```

using single subscriptions
```typescript
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Globals } from 'ngx-globals';

@Component({
    template: '<p>Component Contents</p>'
})

export class SomeComponent implements OnInit, OnDestroy {

	Subscriber:Subscription;

	constructor( private Globals:Globals ) {}

	ngOnInit(){

		this.Subscriber = this.Globals.observe('SomeGlobalParam')
			.subscribe((Data) => {

				//Do Something

			});

	}

	ngOnDestroy(){

		this.Subscriber.unsubscribe();

	}

}
```

using multiple subscriptions
```typescript
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Globals } from 'ngx-globals';

@Component({
    template: '<p>Component Contents</p>'
})

export class SomeComponent implements OnInit, OnDestroy {

	constructor( private Globals:Globals ) {}

	ngOnInit(){

		this.Globals.subscribe(this.Globals.observe('SomeGlobalParam')
			.subscribe((Data) => {

				//Do Something

			});
		);

		this.Globals.subscribe(this.Globals.observe('SomeOtherParam')
			.subscribe((Data) => {

				//Do Something

			});
		);

	}

	ngOnDestroy(){

		this.Globals.unsubscribe();

	}

}
```